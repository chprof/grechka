$(document).ready(function() {
	svg4everybody();
	$('[data-toggle]').click(function() {
		if ( $(this).data('toggle') == 'toggleVisible' ) {
			var target = $(this).data('target');
			$('[data-id="'+target+'"]').toggleClass('visible');
		}
		if ( $(this).data('toggle') == 'anchor' ) {
			if ( $(window).width() > 767 ) {
				var target = $($(this).attr('href')).offset().top;
			} else {
				var target = $($(this).attr('href')).offset().top - ( $('.header').outerHeight() - 1 );
			}
			// console.log($('.header').outerHeight());
			$('body,html').animate({
				scrollTop: target
			}, 500);
		}
	})


	function getClip() {
		document.getElementById("copyButton").addEventListener("click", function() {
			copyToClipboard(document.getElementById("copyTarget"));
		});

		function copyToClipboard(elem) {
			// create hidden text element, if it doesn't already exist
			var targetId = "_hiddenCopyText_";
			var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
			var origSelectionStart, origSelectionEnd;
			if (isInput) {
				// can just use the original source element for the selection and copy
				target = elem;
				origSelectionStart = elem.selectionStart;
				origSelectionEnd = elem.selectionEnd;
			} else {
				// must use a temporary form element for the selection and copy
				target = document.getElementById(targetId);
				if (!target) {
					var target = document.createElement("textarea");
					target.style.position = "absolute";
					target.style.left = "-9999px";
					target.style.top = "0";
					target.id = targetId;
					document.body.appendChild(target);
				}
				target.textContent = elem.textContent;
			}
			// select the content
			var currentFocus = document.activeElement;
			target.focus();
			target.setSelectionRange(0, target.value.length);
			
			// copy the selection
			var succeed;
			try {
				succeed = document.execCommand("copy");
			} catch(e) {
				succeed = false;
			}
			// restore original focus
			if (currentFocus && typeof currentFocus.focus === "function") {
				currentFocus.focus();
			}
			
			if (isInput) {
				// restore prior selection
				elem.setSelectionRange(origSelectionStart, origSelectionEnd);
			} else {
				// clear temporary content
				target.textContent = "";
			}
			return succeed;
		}
	};
	getClip()
	// $('.checkbox-input').change(function() {
	// 	if ( !$(this).prop('checked') ) {
	// 		$(this).closest('form').find('button[type="submit"]').attr('disabled', true)
	// 	} else {
	// 		$(this).closest('form').find('button[type="submit"]').removeAttr('disabled')
	// 	}
	// })




	// new WOW().init({
	// 	mobile: false,
	// 	resetAnimation: false,
	// });
})
